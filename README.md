# kagauge
A simple yet powerfull D3 based gauge chart utility.

## Features
* Label with ellipses support for very long text.
* Fully configurable chart elements as CSS class.
* 3 zone support (green, orange, red). Can be easily extended to more.

## Usage
Just create  a new object of KaGauge (passing dom node id and config) and call render() method thereafter to update gauge value as below:
<pre>
var g = new KaGauge('chart', config);
setInterval(function(){
	g.render(Math.floor(Math.random()*100));
}, 1000);
</pre>

