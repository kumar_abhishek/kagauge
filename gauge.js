/*
The MIT License (MIT)

Copyright (c) 2016 Kumar Abhishek

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/

function KaGauge(placeholderName, configuration)
{
	var self = this, firstTime = true;
	this.placeholderName = placeholderName;
	this.chart = document.querySelector('#'+placeholderName);
	this.config = configuration;
	this.config.label = this.config.label || '';
	this.config.value = this.config.value || 0;
	this.config.size = getSize();
	this.config.raduis = this.config.size * 0.97 / 2;
	this.config.cx = this.config.size / 2;
	this.config.cy = this.config.size / 2;
	this.config.min = undefined != configuration.min ? configuration.min : 0; 
	this.config.max = undefined != configuration.max ? configuration.max : 100; 
	this.config.range = this.config.max - this.config.min;
	this.config.majorTicks = configuration.majorTicks || 5;
	this.config.minorTicks = configuration.minorTicks || 2;
	this.config.transitionDuration = configuration.transitionDuration || 500;
	this.ticks = [];
	this.majorTickLabels = [];
	
	
	addEventListener('resize', function(){
		self.config.size = getSize();
		self.config.raduis = self.config.size * 0.97 / 2;
		self.config.cx = self.config.size / 2;
		self.config.cy = self.config.size / 2;
		init();
	});
	
	function getSize(){
		var chart = document.querySelector('#'+self.placeholderName);
		return Math.min(chart.clientHeight, chart.clientWidth);
	}
	
	function renderDial(){
		self.dialCircle1 = self.dialCircle1 || self.body.append("svg:circle").attr("class", "outer-circle");
		self.dialCircle1.attr("cx", self.config.cx).attr("cy", self.config.cy).attr("r", self.config.raduis);			
		self.dialCircle2 = self.dialCircle2 || self.body.append("svg:circle").attr("class", "inner-circle");
		self.dialCircle2.attr("cx", self.config.cx).attr("cy", self.config.cy).attr("r", 0.9 * self.config.raduis);
	}
	
	function drawBand(start, end, clazz){
		var config = self.config, body = self.body;
		start = config.min + config.range*start;
		end = config.min + config.range*end;
		if (0 >= end - start) return;
		
		self.config.zones[clazz].band = self.config.zones[clazz].band || body.append("svg:path");  
		self.config.zones[clazz].band
			.attr("class", clazz)
			.attr("d", d3.svg.arc()
				.startAngle(valueToRadians(start))
				.endAngle(valueToRadians(end))
				.innerRadius(0.65 * config.raduis)
				.outerRadius(0.85 * config.raduis))
			.attr("transform", function() { return "translate(" + self.config.cx + ", " + self.config.cy + ") rotate(270)" });
	}
	
	function renderZones(){
		for(var zone in self.config.zones){
			drawBand(self.config.zones[zone].from, self.config.zones[zone].to, zone);	
		}
	}
	
	function valueToDegrees(value){
		return value / self.config.range * 270 - (self.config.min / self.config.range * 270 + 45);
	}
	
	function valueToRadians(value){
		return valueToDegrees(value) * Math.PI / 180;
	}
	
	function valueToPoint(value, factor){
		return {
			x: self.config.cx - self.config.raduis * factor * Math.cos(valueToRadians(value)),
			y: self.config.cy - self.config.raduis * factor * Math.sin(valueToRadians(value))
		};
	}
	
	function renderLabel(){
		self.fo = self.fo || self.body.append("foreignObject");
		self.fo.attr("width", self.config.size)
			.attr("height", self.config.size)
			.append("xhtml:body")
			.html('<div class="label" title="'+self.config.label+'">'+self.config.label+'</div>');
	}
	
	function buildPointerPath(value)
	{
		function valueToPointRelative(value, factor){
			var point = valueToPoint(value, factor);
			point.x -= self.config.cx;
			point.y -= self.config.cy;
			return point;
		}
		
		var delta = self.config.range / 13,
		head = valueToPointRelative(value, 0.85),
		head1 = valueToPointRelative(value - delta, 0.12),
		head2 = valueToPointRelative(value + delta, 0.12),
		tailValue = value - (self.config.range * (1/(270/360)) / 2),
		tail = valueToPointRelative(tailValue, 0.28),
		tail1 = valueToPointRelative(tailValue - delta, 0.12),
		tail2 = valueToPointRelative(tailValue + delta, 0.12);
		
		return [head, head1, tail2, tail, tail1, head2, head];
	}
	

	function renderTickLines(value, type){
		var point1 = valueToPoint(value, 0.75);
		var point2 = valueToPoint(value, 0.85);
		
		var tick = self.body.append("svg:line")
					.attr("x1", point1.x)
					.attr("y1", point1.y)
					.attr("x2", point2.x)
					.attr("y2", point2.y)
					.attr('class', type+'-tick');
		self.ticks.push(tick);
	}
	
	function renderMajorTickLabel(major){
		var fontSize = Math.round(self.config.size / 16), point = valueToPoint(major, 0.55);
		if ( true || major == self.config.min || major == self.config.max){
			var tickLabel = self.body.append("svg:text")
				.attr("x", point.x)
				.attr("y", point.y)
				.attr("dy", fontSize / 3)
				.attr("text-anchor", "middle")
				.attr('class', 'major-tick-label')
				.text(Math.floor(major))
				.style("font-size", fontSize/1.7 + "px")
			self.majorTickLabels.push(tickLabel); 
		}
	}
	
	function renderTicks(){
		var major = 0, minor = 0,  majorDelta = self.config.range / (self.config.majorTicks - 1);
		self.ticks.forEach(function(t){
			t.remove();
		});
		self.majorTickLabels.forEach(function(t){
			t.remove();
		});
		for(major = self.config.min; major <= self.config.max; major += majorDelta){
			var minorDelta = majorDelta / self.config.minorTicks, minorLimit = Math.min(major + majorDelta, self.config.max);
			for (minor = major + minorDelta; minor < minorLimit; minor += minorDelta){
				renderTickLines(minor, 'minor');
			}
			renderTickLines(major, 'major');
			renderMajorTickLabel(major);
		}
	}
	
	function renderNeedle(){
		self.pointerContainer = self.pointerContainer || self.body.append("svg:g").attr("class", "pointerContainer");
		self.pointerContainer.html('');
		
		var midValue = (self.config.min + self.config.max) / 2,
			pointerPath = buildPointerPath(midValue),
			pointerLine = d3.svg.line().x(function(d) { return d.x }).y(function(d) { return d.y }).interpolate("basis");
		
		self.pointerContainer.selectAll("path").data([pointerPath]).enter().append("svg:path").attr("d", pointerLine).attr('class', 'needle');
		
		self.pointerContainer.append("svg:circle")
							.attr("cx", self.config.cx)
							.attr("cy", self.config.cy)
							.attr("r", 0.12 * self.config.raduis)
							.attr('class', 'needle-container');
		
		var fontSize = Math.round(self.config.size / 10);
		self.pointerContainer.selectAll("text")
							.data([midValue])
							.enter()
								.append("svg:text")
									.attr("x", self.config.cx)
									.attr("y", self.config.size - self.config.cy / 4 - fontSize)
									.attr("dy", fontSize / 2)
									.attr("class", 'current-value')
									.attr("text-anchor", "middle")
									.style("font-size", fontSize*1.5 + 'px');
		if(firstTime){
			self.render(self.config.min, 0);
			self.render(self.config.value);
			firstTime = false;
		}
		else{
			self.render(self.config.value);
		}
	}
	
	function init(){
		self.body = self.body || d3.select("#" + self.placeholderName).append("svg:svg").attr("class", "gauge");
		self.body.attr("width", self.config.size);
		renderDial();	
		renderZones();
		renderLabel();
		renderTicks();
		renderNeedle();
	}
	
	this.render = function(value, transitionDuration){
		var pointerContainer = this.body.select(".pointerContainer");
		pointerContainer.selectAll("text").text(Math.round(value));
		var pointer = pointerContainer.selectAll("path");
		if(!firstTime){
			self.config.value = value;
		}
		
		pointer.transition()
			.duration(undefined != transitionDuration ? transitionDuration : this.config.transitionDuration)
			.attrTween("transform", function(){
				var pointerValue = value;
				if(value > self.config.max){
					pointerValue = self.config.max + 0.02*self.config.range;	
				}
				else if (value < self.config.min){
					pointerValue = self.config.min - 0.02*self.config.range;	
				}
				var targetRotation = (valueToDegrees(pointerValue) - 90),
				currentRotation = self._currentRotation || targetRotation;
				self._currentRotation = targetRotation;
				
				return function(step) {
					var rotation = currentRotation + (targetRotation-currentRotation)*step;
					return "translate(" + self.config.cx + ", " + self.config.cy + ") rotate(" + rotation + ")"; 
				}
			});
	}
	
	init(true);
}